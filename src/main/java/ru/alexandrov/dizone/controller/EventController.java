package ru.alexandrov.dizone.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.alexandrov.dizone.dao.entity.Event;
import ru.alexandrov.dizone.dao.repository.EventRepository;
import ru.alexandrov.dizone.request.EventRequest;
import ru.alexandrov.dizone.request.StatisticRequest;
import ru.alexandrov.dizone.response.EventResponse;
import ru.alexandrov.dizone.response.StatisticResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@RestController
@RequiredArgsConstructor
public class EventController {
    private final EventRepository eventRepository;

    @PostMapping("/visit-site")
    public @ResponseBody
    ResponseEntity<EventResponse> getEvents(@RequestBody EventRequest request) {
        Date currentDate = new Date();
        Long pageId = request.getPageId();
        Long userId = request.getUserId();
        if (pageId == null || userId == null) {
            log.error("Bad request, need page id and user id, {} {}", request.getPageId(), request.getUserId());
            return ResponseEntity.badRequest().build();
        }
        eventRepository.save(
                new Event()
                        .setDate(currentDate)
                        .setPageId(request.getPageId())
                        .setUserId(request.getUserId()));
        EventResponse response = new EventResponse()
                .setVisits(eventRepository.getCountOfVisitsByDate(currentDate))
                .setUniqueUsers(eventRepository.getCountOfUniqueVisitsByDate(currentDate));
        return ResponseEntity.ok(response);
    }

    @PostMapping("/get-stats")
    public @ResponseBody
    ResponseEntity<StatisticResponse> getStats(@RequestBody StatisticRequest request) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        if (request.getDateFrom() == null || request.getDateTo() == null) {
            log.error("Bad date in request, need XX.XX.XXXX format, {} {}", request.getDateFrom(), request.getDateTo());
            return ResponseEntity.badRequest().build();
        }
        Date dateFrom = null;
        Date dateTo = null;
        try {
            dateFrom = formatter.parse(request.getDateFrom());
            dateTo = formatter.parse(request.getDateTo());
        } catch (ParseException e) {
            log.error("Bad date in request, need XX.XX.XXXX format, {} {}", request.getDateFrom(), request.getDateTo());
            return ResponseEntity.badRequest().build();
        }
        StatisticResponse response = new StatisticResponse()
                .setVisits(eventRepository.getCountOfVisitsByDateRange(dateFrom, dateTo))
                .setUniqueUsers(eventRepository.getCountOfUniqueVisitsByDateRange(dateFrom, dateTo))
                .setRegularUsers(eventRepository.getCountOfRegularVisitsByDateRange(dateFrom, dateTo));
        return ResponseEntity.ok(response);
    }
}