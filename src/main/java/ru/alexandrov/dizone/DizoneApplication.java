package ru.alexandrov.dizone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DizoneApplication {


    public static void main(String[] args) {
        SpringApplication.run(DizoneApplication.class, args);
    }

}
