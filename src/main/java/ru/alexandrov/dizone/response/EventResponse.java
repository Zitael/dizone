package ru.alexandrov.dizone.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class EventResponse {
    @JsonProperty("visits")
    private Long visits;

    @JsonProperty("unique_users")
    private Long uniqueUsers;
}
