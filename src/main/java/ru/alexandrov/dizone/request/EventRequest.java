package ru.alexandrov.dizone.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class EventRequest {
    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("page_id")
    private Long pageId;
}