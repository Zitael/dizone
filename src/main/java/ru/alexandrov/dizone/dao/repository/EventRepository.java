package ru.alexandrov.dizone.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.alexandrov.dizone.dao.entity.Event;

import java.util.Date;

public interface EventRepository extends JpaRepository<Event, Long> {

    @Query("SELECT COUNT(e) FROM Event e WHERE e.date = (:date)")
    Long getCountOfVisitsByDate(@Param("date") Date date);

    @Query("SELECT COUNT (DISTINCT e.userId) FROM Event e WHERE e.date = (:date)")
    Long getCountOfUniqueVisitsByDate(@Param("date") Date date);

    @Query("SELECT COUNT(e) FROM Event e WHERE e.date BETWEEN (:dateFrom) AND (:dateTo)")
    Long getCountOfVisitsByDateRange(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);

    @Query("SELECT COUNT(DISTINCT e.userId) FROM Event e WHERE e.date BETWEEN (:dateFrom) AND (:dateTo)")
    Long getCountOfUniqueVisitsByDateRange(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);

    @Query("SELECT COUNT(DISTINCT e.userId) " +
            "FROM Event e " +
            "WHERE 10 <= (" +
            "SELECT COUNT(DISTINCT a.pageId) FROM Event a WHERE a.userId = e.userId) " +
            "AND e.date BETWEEN (:dateFrom) AND (:dateTo)")
    Long getCountOfRegularVisitsByDateRange(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);

}
