package ru.alexandrov.dizone;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DizoneApplicationTests {
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void contextLoads() {
    }

    @Test
    public void visitSite_WhenOk() throws Exception {
        mockMvc.perform(
                post("/visit-site")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n\"user_id\" : \"4\" ,\n\"page_id\" : \"4\"\n}"
                        ))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        //"        \"visits\": 0,\n" +
                        "        \"unique_users\": 1\n" +
                        "    }\n"));
    }

    @Test
    public void visitSite_WhenBadRequest() throws Exception {
        mockMvc.perform(
                post("/visit-site")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n\"userid\" : \"string\" ,\n\"pageid\" : \"4\"\n}"
                        ))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getStats_WhenOk() throws Exception {
        mockMvc.perform(
                post("/get-stats")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n\"date_from\" : \"01.01.1990\" ,\n\"date_to\" : \"18.04.2010\"\n}"
                        ))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "        \"visits\": 11,\n" +
                        "        \"unique_users\": 2,\n" +
                        "        \"regular_users\": 1\n" +
                        "    }\n"));
    }

    @Test
    public void getStats_WhenBadRequest() throws Exception {
        mockMvc.perform(
                post("/get-stats")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n\"userid\" : \"string\" ,\n\"pageid\" : \"4\"\n}"
                        ))
                .andExpect(status().isBadRequest());
    }
}
